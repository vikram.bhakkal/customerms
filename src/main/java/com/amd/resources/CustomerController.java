package com.amd.resources;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amd.model.Customer;
import com.amd.services.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	CustomerService customerService;
	
	@RequestMapping(value ="/customer/all", method = RequestMethod.GET)
	public List<Customer> getCustomers(){
		System.out.println("Get all customer details");
		return customerService.getCustomers();
	}
	@RequestMapping(value ="/customer/{id}", method = RequestMethod.GET)
	public Optional<Customer> getCustomerById(@PathVariable int custId) throws Exception{
		Optional<Customer> fatchCust = customerService.getCustomerById(custId);
		if(!fatchCust.isPresent())
			throw new Exception("Couldn't find any customer with id "+custId);
		return customerService.getCustomerById(custId);
	}
	@RequestMapping(value ="/customer/add",consumes = "application/json",  method = RequestMethod.POST)
	 public Customer addCustomer(@RequestBody Customer cust) {
		 return customerService.addCustomer(cust);
	 }
	
	@RequestMapping(value ="/customer/update/{id}", method =RequestMethod.PUT)
	public Customer updateCustomer(@RequestBody Customer cust, @PathVariable int id) throws Exception {
		Optional<Customer> fatchCust = customerService.getCustomerById(id);
		if(!fatchCust.isPresent())
			throw new Exception("Couldn't find any customer with id "+id);
		if(null ==cust.getCustName() || cust.getCustName().isEmpty() )
			cust.setCustName(fatchCust.get().getCustName());
		cust.setCustId(id);
		return customerService.updateCustomer(cust);
	}
	
	@RequestMapping(value="/customer/deleteall", method = RequestMethod.DELETE)
	 public void removeAll() {
		 customerService.deleteAllCustomers();
	 }
	
	public void removeCustomerById(@RequestBody int id) throws Exception {
		Optional<Customer> fatchCust = customerService.getCustomerById(id);
		if(!fatchCust.isPresent())
			throw new Exception("Couldn't find any customer with id "+id);
		customerService.deleteCustomerById(id);
	}
}

