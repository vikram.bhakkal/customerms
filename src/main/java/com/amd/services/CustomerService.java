package com.amd.services;

import java.util.List;
import java.util.Optional;

import com.amd.model.Customer;

public interface CustomerService {

	public List<Customer> getCustomers();

	public Optional<Customer> getCustomerById(int id);

	public Customer addCustomer(Customer customer);

	public Customer updateCustomer(Customer customer);

	public void deleteCustomerById(int id);

	public void deleteAllCustomers();

}
