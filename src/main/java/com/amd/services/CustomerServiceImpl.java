package com.amd.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amd.dao.CustomerRepository;
import com.amd.model.Customer;
@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository dao;
	@Override
	public List<Customer> getCustomers() {
		return dao.findAll();
	}

	@Override
	public Optional<Customer> getCustomerById(int id) {
		// TODO Auto-generated method stub
		return dao.findById(id);
	}

	@Override
	public Customer addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return dao.save(customer);
	}

	@Override
	public Customer updateCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return dao.save(customer);
	}

	@Override
	public void deleteCustomerById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void deleteAllCustomers() {
		dao.deleteAll();
		
	}

}
